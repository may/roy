use std::collections::HashMap;

#[allow(dead_code)]
struct Context {
    stack: Vec<i64>,
    strings: HashMap<i64, String>,
    quotes: HashMap<i64, fn(&mut Context)>,
    string_counter: i64,
    quote_counter: i64,
}

impl Context {
    fn new() -> Self {
        Self {
            stack: vec![],
            strings: HashMap::new(),
            quotes: HashMap::new(),
            string_counter: 0,
            quote_counter: 0,
        }
    }
    fn push(&mut self, data: i64) {
        self.stack.push(data);
    }
    fn pop(&mut self) -> i64 {
        self.stack.pop().unwrap()
    }
}

fn main() {
    let mut ctx = Context::new();
    let ctx = &mut ctx;
    ctx.strings.insert(ctx.string_counter, "Hello world!".to_string());
    ctx.push(ctx.string_counter);
    ctx.string_counter += 1;
    let mut MESSAGE = ctx.pop();
    ctx.push(MESSAGE);
    let temp = ctx.pop();
    let value = ctx.strings.get(&temp).unwrap();
    println!("{}", value);
    ctx.push(MESSAGE);
    let temp = ctx.pop();
    let value = ctx.strings.get(&temp).unwrap();
    println!("{}", value);
}
