#[derive(Debug, Clone)]
pub enum Token {
    IntLiteral(i64),
    FloatLiteral(f64),
    StringLiteral(String),
    OpenBracket,
    CloseBracket,
    Raw(String),
    If,
    Fn,
    For,
    Set,
    Push,
    Pop,
}

#[derive(Debug, Clone)]
pub enum Segment {
    Single(Token),
    Block(Vec<Segment>),
}

#[derive(Debug, Clone)]
pub enum Operator {
    Add,
    Subtract,
    Multiply,
    Divide,
    GreaterThan,
    LessThan,
    GreaterThanOrEqual,
    LessThanOrEqual,
    Equal,
    NotEqual,
    Duplicate,
    Swap,
    Drop,
    Output,
    Print,
}

#[derive(Debug, Clone)]
pub enum Node {
    PushInt(i64),
    PushFloat(f64),
    PushString(String),
    PushQuote(Vec<Node>),
    RunOperator(Operator),
    CallFunction(String),
    DefineFunction(String, Vec<Node>),
    If(Vec<Node>, Vec<Node>),
    For(Vec<Node>),
    Set(String),
    Push(String),
    Pop(String),
}
