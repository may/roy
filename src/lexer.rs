use crate::Token;

pub struct Lexer {
    tokens: Vec<Token>,
    chars: Vec<char>,
    i: usize,
}

impl Lexer {
    pub fn new(text: String) -> Self {
        Self {
            tokens: vec![],
            chars: text.chars().collect(),
            i: 0,
        }
    }

    fn next(&self) -> Option<&char> {
        self.chars.get(self.i)
    }

    fn build(&mut self, f: impl Fn(Option<&char>) -> bool) -> String {
        let mut builder = String::new();
        while f(self.next()) {
            builder.push(*self.next().unwrap());
            self.i += 1;
        }
        builder
    }

    pub fn lex(mut self) -> Vec<Token> {
        while let Some(chr) = self.next() {
            match chr {
                ' ' | '\t' | '\n' => self.i += 1,
                '(' => {
                    self.tokens.push(Token::OpenBracket);
                    self.i += 1;
                }
                ')' => {
                    self.tokens.push(Token::CloseBracket);
                    self.i += 1;
                }
                '"' => {
                    self.i += 1;
                    let text = self.build(|c| *c.unwrap() != '"');
                    self.tokens.push(Token::StringLiteral(text));
                    self.i += 1;
                }
                '0'..='9' => {
                    let text = self.build(|c| matches!(c, Some('0'..='9' | '.')));
                    if let Ok(as_int) = text.parse::<i64>() {
                        self.tokens.push(Token::IntLiteral(as_int));
                    } else if let Ok(as_float) = text.parse::<f64>() {
                        self.tokens.push(Token::FloatLiteral(as_float));
                    } else {
                        panic!("Failed to parse number: {text}");
                    }
                }
                _ => {
                    let text = self.build(|c| ![' ', '\t', '\n', '(', ')'].contains(c.unwrap()));
                    match text.as_str() {
                        "if" => self.tokens.push(Token::If),
                        "fn" => self.tokens.push(Token::Fn),
                        "for" => self.tokens.push(Token::For),
                        "set" => self.tokens.push(Token::Set),
                        "push" => self.tokens.push(Token::Push),
                        "pop" => self.tokens.push(Token::Pop),
                        _ => self.tokens.push(Token::Raw(text)),
                    }
                }
            }
        }
        self.tokens
    }
}
