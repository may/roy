use clap::Parser;
use std::fs;

mod compiler;
mod include;
mod lexer;
mod parser;

pub use include::*;

#[derive(Parser, Debug)]
struct Args {
    source: String,
    destination: String,
}

fn main() {
    let args = Args::parse();
    let text = fs::read_to_string(args.source).unwrap();
    let tokens = lexer::Lexer::new(text).lex();
    let segments = parser::segment(tokens);
    let nodes = parser::parse(segments);
    let text = compiler::compile(nodes);

    fs::write(args.destination, text).unwrap();
}
