use crate::{Node, Operator, Segment, Token};

pub fn segment(tokens: Vec<Token>) -> Vec<Segment> {
    let mut result = vec![];
    let mut builder = vec![];
    let mut level = 0;

    for token in tokens {
        match token {
            Token::OpenBracket => {
                level += 1;
                if level != 1 {
                    builder.push(token);
                }
            }
            Token::CloseBracket => {
                level -= 1;
                if level == 0 {
                    let inner = segment(builder.clone());
                    result.push(Segment::Block(inner));
                    builder.clear();
                } else {
                    builder.push(token);
                }
            }
            _ => {
                if level == 0 {
                    result.push(Segment::Single(token));
                } else {
                    builder.push(token);
                }
            }
        }
    }
    result
}

fn parse_raw(text: String) -> Node {
    match text.as_str() {
        "+" => Node::RunOperator(Operator::Add),
        "-" => Node::RunOperator(Operator::Subtract),
        "*" => Node::RunOperator(Operator::Multiply),
        "/" => Node::RunOperator(Operator::Divide),
        ">" => Node::RunOperator(Operator::GreaterThan),
        "<" => Node::RunOperator(Operator::LessThan),
        ">=" => Node::RunOperator(Operator::GreaterThanOrEqual),
        "<=" => Node::RunOperator(Operator::LessThanOrEqual),
        "==" => Node::RunOperator(Operator::Equal),
        "!=" => Node::RunOperator(Operator::NotEqual),
        "." => Node::RunOperator(Operator::Output),
        "print" => Node::RunOperator(Operator::Print),
        "dup" => Node::RunOperator(Operator::Duplicate),
        "swap" => Node::RunOperator(Operator::Swap),
        "drop" => Node::RunOperator(Operator::Drop),
        _ => Node::CallFunction(text),
    }
}

pub fn parse(segments: Vec<Segment>) -> Vec<Node> {
    let mut result = vec![];
    let mut segments = segments.iter();

    while let Some(segment) = segments.next() {
        match segment.clone() {
            Segment::Single(Token::IntLiteral(num)) => result.push(Node::PushInt(num)),
            Segment::Single(Token::FloatLiteral(num)) => result.push(Node::PushFloat(num)),
            Segment::Single(Token::StringLiteral(text)) => result.push(Node::PushString(text)),
            Segment::Single(Token::Raw(text)) => result.push(parse_raw(text)),
            Segment::Single(Token::Fn) => {
                let name = segments.next().unwrap();
                let code = segments.next().unwrap();
                if let Segment::Single(Token::Raw(name)) = name {
                    if let Segment::Block(code) = code {
                        let function_name = name.to_string();
                        let function_body = parse(code.clone());
                        result.push(Node::DefineFunction(function_name, function_body));
                    }
                }
            }
            Segment::Single(Token::Set) => {
                let name = segments.next().unwrap();
                if let Segment::Single(Token::Raw(name)) = name {
                    result.push(Node::Set(name.to_string()));
                }
            }
            Segment::Single(Token::Push) => {
                let name = segments.next().unwrap();
                if let Segment::Single(Token::Raw(name)) = name {
                    result.push(Node::Push(name.to_string()));
                }
            }
            Segment::Single(Token::Pop) => {
                let name = segments.next().unwrap();
                if let Segment::Single(Token::Raw(name)) = name {
                    result.push(Node::Pop(name.to_string()));
                }
            }
            Segment::Single(Token::If) => {
                let condition = segments.next().unwrap();
                let code = segments.next().unwrap();
                if let (Segment::Block(condition), Segment::Block(code)) = (condition, code) {
                    let if_condition = parse(condition.clone());
                    let if_body = parse(code.clone());
                    result.push(Node::If(if_condition, if_body));
                }
            }
            Segment::Single(Token::For) => {
                let code = segments.next().unwrap();
                if let Segment::Block(code) = code {
                    let for_body = parse(code.clone());
                    result.push(Node::For(for_body));
                }
            }
            Segment::Single(Token::OpenBracket | Token::CloseBracket) => {
                panic!("Unreachable, brackets found inside parser")
            }
            Segment::Block(inner) => {
                let code = parse(inner);
                result.push(Node::PushQuote(code));
            }
        }
    }

    result
}
