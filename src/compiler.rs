use crate::{Node, Operator};

fn binary_op(op: &str, body: &mut String) {
    body.push_str("    let temp_1 = ctx.pop();\n");
    body.push_str("    let temp_2 = ctx.pop();\n");
    body.push_str(&format!("    ctx.push(temp_1 {} temp_2);\n", op));
}

fn binary_eq(op: &str, body: &mut String) {
    body.push_str("    let temp_1 = ctx.pop();\n");
    body.push_str("    let temp_2 = ctx.pop();\n");
    body.push_str(&format!("    ctx.push((temp_2 {} temp_1) as i64);\n", op));
}

fn build_operator(op: Operator, body: &mut String) {
    match op {
        Operator::Add => binary_op("+", body),
        Operator::Subtract => binary_op("-", body),
        Operator::Multiply => binary_op("*", body),
        Operator::Divide => binary_op("/", body),
        Operator::GreaterThan => binary_eq(">", body),
        Operator::LessThan => binary_eq("<", body),
        Operator::GreaterThanOrEqual => binary_eq(">=", body),
        Operator::LessThanOrEqual => binary_eq("<=", body),
        Operator::Equal => binary_eq("==", body),
        Operator::NotEqual => binary_eq("!=", body),
        Operator::Duplicate => {
            body.push_str("    let temp = ctx.pop();\n");
            body.push_str("    ctx.push(temp);\n");
            body.push_str("    ctx.push(temp);\n");
        }
        Operator::Swap => {
            body.push_str("    let temp_1 = ctx.pop();\n");
            body.push_str("    let temp_2 = ctx.pop();\n");
            body.push_str("    ctx.push(temp_1);\n");
            body.push_str("    ctx.push(temp_2);\n");
        }
        Operator::Output => {
            body.push_str("    let temp = ctx.pop();\n");
            body.push_str("    println!(\"{}\", temp);\n");
        }
        Operator::Drop => {
            body.push_str("    ctx.pop();\n");
        }
        Operator::Print => {
            body.push_str("    let temp = ctx.pop();\n");
            body.push_str("    let value = ctx.strings.get(&temp).unwrap();\n");
            body.push_str("    println!(\"{}\", value);\n");
        }
    }
}

fn walk(nodes: Vec<Node>, body: &mut String, funcs: &mut Vec<String>, counter: &mut i64) {
    for node in nodes {
        match node {
            Node::PushInt(num) => body.push_str(&format!("    ctx.push({});\n", num)),
            Node::PushFloat(_) => todo!(),
            Node::PushString(text) => {
                body.push_str(&format!(
                    "    ctx.strings.insert(ctx.string_counter, \"{}\".to_string());\n",
                    text
                ));
                body.push_str("    ctx.push(ctx.string_counter);\n");
                body.push_str("    ctx.string_counter += 1;\n");
            }
            Node::PushQuote(code) => {
                let mut quote = String::new();
                quote.push_str(&format!("fn _anon_{}(ctx: &mut Context) {{\n", counter));
                walk(code, &mut quote, funcs, counter);
                quote.push_str("}\n");
                funcs.push(quote);
                body.push_str(&format!(
                    "    ctx.quotes.insert(ctx.quote_counter, _anon_{});\n",
                    counter
                ));
                *counter += 1;
                body.push_str("    ctx.push(ctx.quote_counter);\n");
                body.push_str("    ctx.quote_counter += 1;\n");
            }
            Node::DefineFunction(name, code) => {
                let mut quote = String::new();
                quote.push_str(&format!("fn _defined_{}(ctx: &mut Context) {{\n", name));
                walk(code, &mut quote, funcs, counter);
                quote.push_str("}\n");
                funcs.push(quote);
            }
            Node::CallFunction(name) => {
                body.push_str(&format!("    _defined_{}(ctx);\n", name));
            }
            Node::If(condition, code) => {
                walk(condition, body, funcs, counter);
                let mut inner_code = String::new();
                walk(code, &mut inner_code, funcs, counter);
                body.push_str("    if ctx.pop() == 1 {\n");
                body.push_str(&inner_code);
                body.push_str("    }\n");
            }
            Node::For(code) => {
                body.push_str("    for _i in 0..(ctx.pop()) {\n");
                body.push_str("        ctx.push(_i);\n");
                walk(code, body, funcs, counter);
                body.push_str("    }\n");
            }
            Node::Set(name) => {
                body.push_str(&format!("    let mut {} = ctx.pop();\n", name));
            }
            Node::Pop(name) => {
                body.push_str(&format!("    {} = ctx.pop();\n", name));
            }
            Node::Push(name) => {
                body.push_str(&format!("    ctx.push({});\n", name));
            }
            Node::RunOperator(op) => build_operator(op, body),
        }
    }
}

pub fn compile(nodes: Vec<Node>) -> String {
    let mut result = String::new();

    let mut body = String::new();
    let mut funcs = vec![];
    let mut counter = 0;
    walk(nodes, &mut body, &mut funcs, &mut counter);

    result.push_str("use std::collections::HashMap;\n");
    result.push_str("\n");
    result.push_str("#[allow(dead_code)]\n");
    result.push_str("struct Context {\n");
    result.push_str("    stack: Vec<i64>,\n");
    result.push_str("    strings: HashMap<i64, String>,\n");
    result.push_str("    quotes: HashMap<i64, fn(&mut Context)>,\n");
    result.push_str("    string_counter: i64,\n");
    result.push_str("    quote_counter: i64,\n");
    result.push_str("}\n");
    result.push_str("\n");
    result.push_str("impl Context {\n");
    result.push_str("    fn new() -> Self {\n");
    result.push_str("        Self {\n");
    result.push_str("            stack: vec![],\n");
    result.push_str("            strings: HashMap::new(),\n");
    result.push_str("            quotes: HashMap::new(),\n");
    result.push_str("            string_counter: 0,\n");
    result.push_str("            quote_counter: 0,\n");
    result.push_str("        }\n");
    result.push_str("    }\n");
    result.push_str("    fn push(&mut self, data: i64) {\n");
    result.push_str("        self.stack.push(data);\n");
    result.push_str("    }\n");
    result.push_str("    fn pop(&mut self) -> i64 {\n");
    result.push_str("        self.stack.pop().unwrap()\n");
    result.push_str("    }\n");
    result.push_str("}\n");
    result.push_str("\n");

    for func in funcs {
        result.push_str(&func);
        result.push_str("\n");
    }

    result.push_str("fn main() {\n");
    result.push_str("    let mut ctx = Context::new();\n");
    result.push_str("    let ctx = &mut ctx;\n");
    result.push_str(&body);
    result.push_str("}\n");

    result
}
